/*
  Warnings:

  - Added the required column `slideshow_id` to the `slides` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "slides" ADD COLUMN     "slideshow_id" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "slideshows" (
    "id" TEXT NOT NULL,

    CONSTRAINT "slideshows_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "slides" ADD CONSTRAINT "slides_slideshow_id_fkey" FOREIGN KEY ("slideshow_id") REFERENCES "slideshows"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
