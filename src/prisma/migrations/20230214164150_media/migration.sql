-- CreateEnum
CREATE TYPE "MediaType" AS ENUM ('IMAGE', 'VIDEO');

-- CreateTable
CREATE TABLE "slides" (
    "id" TEXT NOT NULL,
    "src" TEXT NOT NULL,
    "type" "MediaType" NOT NULL,
    "alt" TEXT NOT NULL,
    "interval" INTEGER,

    CONSTRAINT "slides_pkey" PRIMARY KEY ("id")
);
