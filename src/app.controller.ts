import {
  Get,
  Controller,
  Render,
  Param,
  Res,
  Post,
  Body,
  Patch,
  Delete,
  NotFoundException,
  HttpCode,
} from '@nestjs/common';
import { Response } from 'express';
import { AppService } from './app.service';
import { CreateSlideshow } from './types/create-slideshow.type';
import { UpdateSlideshow } from './types/update-slideshow.type';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  root(@Res() res: Response) {
    return res.redirect('/slideshows');
  }

  @Get('/slideshows')
  @Render('index')
  async getSlideshows() {
    const slideshows = await this.appService.getSlideshows();

    return { slideshows: slideshows };
  }

  @Get('/slideshows/:id')
  async getSlideshow(@Res() res: Response, @Param('id') id: string) {
    const slideshow = await this.appService.getSlideshow(id);

    if (!slideshow) {
      return res.redirect('/slideshows');
    }

    return res.render('slideshow', { slideshow: slideshow });
  }

  @Get('/api/slideshows')
  async getSlideshowsApi() {
    const slideshows = await this.appService.getSlideshows();

    return slideshows;
  }

  @Get('/api/slideshows/:id')
  async getSlideshowApi(@Param('id') id: string) {
    const slideshow = await this.appService.getSlideshow(id);

    if (!slideshow) {
      throw new NotFoundException();
    }

    return slideshow;
  }

  @Post('/api/slideshows')
  async createSlideshowApi(@Body() createSlideshow: CreateSlideshow) {
    const newSlideshow = await this.appService.createSlideshow(createSlideshow);

    return newSlideshow;
  }

  @Patch('/api/slideshows/:id')
  async updateSlideshowApi(
    @Param('id') id: string,
    @Body() updateSlideshow: UpdateSlideshow,
  ) {
    try {
      const updatedSlideshow = await this.appService.updateSlideshow(
        id,
        updateSlideshow,
      );

      return updatedSlideshow;
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Delete('/api/slideshows/:id')
  @HttpCode(204)
  async deleteSlideshowApi(@Param('id') id: string) {
    try {
      await this.appService.deleteSlideshow(id);
    } catch (error) {
      throw new NotFoundException();
    }
  }
}
