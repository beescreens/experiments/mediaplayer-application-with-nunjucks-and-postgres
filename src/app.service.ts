import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma/prisma.service';
import { CreateSlideshow } from './types/create-slideshow.type';
import { UpdateSlideshow } from './types/update-slideshow.type';

@Injectable()
export class AppService {
  constructor(private readonly prisma: PrismaService) {}

  async getSlideshows() {
    return await this.prisma.slideshow.findMany({
      include: {
        slides: {
          select: {
            alt: true,
            interval: true,
            src: true,
            type: true,
          },
        },
      },
    });
  }

  async getSlideshow(slideshowId: string) {
    const slideshow = await this.prisma.slideshow.findFirst({
      where: {
        id: {
          equals: slideshowId,
        },
      },
      include: {
        slides: {
          select: {
            alt: true,
            interval: true,
            src: true,
            type: true,
          },
        },
      },
    });

    return slideshow;
  }

  async createSlideshow(createSlideshow: CreateSlideshow) {
    const newSlideshow = await this.prisma.slideshow.create({
      data: {
        slides: {
          create: createSlideshow.slides,
        },
      },
      include: {
        slides: {
          select: {
            alt: true,
            interval: true,
            src: true,
            type: true,
          },
        },
      },
    });

    return newSlideshow;
  }

  async updateSlideshow(slideshowId: string, updateSlideshow: UpdateSlideshow) {
    const updatedSlideshow = await this.prisma.slideshow.update({
      where: {
        id: slideshowId,
      },
      data: {
        slides: {
          deleteMany: {},
          create: updateSlideshow.slides,
        },
      },
      include: {
        slides: {
          select: {
            alt: true,
            interval: true,
            src: true,
            type: true,
          },
        },
      },
    });

    return updatedSlideshow;
  }

  async deleteSlideshow(slideshowId: string) {
    await this.prisma.slideshow.delete({
      where: {
        id: slideshowId,
      },
    });
  }
}
