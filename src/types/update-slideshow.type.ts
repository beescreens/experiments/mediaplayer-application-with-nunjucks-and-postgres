import { Slideshow } from './slideshow.type';

export type UpdateSlideshow = Omit<Slideshow, 'id'>;
