import { MediaType } from '../enums/media-type.enum';

export type Slide = {
  src: string;
  type: MediaType;
  alt: string;
  interval?: number;
};
