import { Slideshow } from './slideshow.type';

export type CreateSlideshow = Omit<Slideshow, 'id'>;
