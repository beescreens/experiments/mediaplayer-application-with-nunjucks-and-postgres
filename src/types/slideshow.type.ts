import { Slide } from './slide.type';

export type Slideshow = {
  id: string;
  slides: Slide[];
};
